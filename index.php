<?php

mb_internal_encoding("UTF-8");

define('ROOT', __DIR__);
define('USER', __DIR__ . '/User');

require_once ROOT . '/EkrilCore/RobotLoader.php';

(new EkrilCore\RobotLoader(USER, ROOT))->visit(ROOT . '/User/Components');

(new EkrilCore\Application())->createContainer(EkrilCore\Services\Inject\DiContainer::class)
    ->chargeContainer(ROOT . '/config')
    ->run();
