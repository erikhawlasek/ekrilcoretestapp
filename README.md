# EkrilCore TestApp

Jedná se o demo aplikaci napsanou ve vlastním,
velmi minimalistickém frameworku založeném na
architektuře MVC. Framework je napsán v jazyce ***PHP***,
samotná stránka je pak oživena díky HTML
v kombinaci s JavaScriptem a styly CSS.

## Instalace

Pro běh následující aplikace je nutný webový
server, doporučuji webový server ***Apache***.
Po přístupu na URL adresu (například virtuálního hosta),
bude aplikace plně dostupná.

## O Frameworku

Framework je složen z view, kontrolérů a modelů.
Kontrolér může mít více view na základě
akcí, které nabízí, a dokáže využívat modely (logiku),
 a data předávat mezi nimi. Na základě
***URL*** potom komponenta ***Router***
zvládne zavolat správný kontrolér a akci.

Veškerá logika uživatele je ve složce ***User***,
 veškeré komponenty potřebné pro běh frameworku
pak ve složce ***EkrilCore***, do této
složky by uživatel neměl zasahovat, pokud
nechce funkcionalitu frameworku měnit.

URL se defaultně mapuje na formát
 ***{controller}/{action}/{id?}***

## O Aplikaci

Jedná se o jednoduchou evidenci filmů s přihlašováním,
 na které je funkčnost frameworku demonstrována.
