<?php

use EkrilCore\Services\Controller;
use EkrilCore\Services\Manage\User;

class BaseController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    public function entryPoint()
    {
        if($this->user->isLoggedIn())
        {
            $this->layoutData['userOpts'] = $this->pageData['userOpts'] = [
                    'logged' =>  true,
                    'credetalias' => [
                        'id' => $this->user->getId(),
                        'role' => $this->user->getRole(),   
                    ],
                    'data' => $this->user->getData()
            ];
        }
    }
}