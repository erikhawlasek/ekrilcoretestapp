<?php

use EkrilCore\Services\Authenticator\IAuthenticator;
use EkrilCore\Services\Security\Password;
use EkrilCore\Services\Storage\Database;
use EkrilCore\Services\Capsulation\User;

class Autentifikator implements IAuthenticator
{
    private $database, $password, $user;

    public function __construct(Database $database, Password $password, User $user)
    {
        $this->database = $database;
        $this->password = $password;
        $this->user = $user;
    }

    public function authenticate($userData)
    {
        list($username, $password) = $userData;

        $assoc = $this->database->query("SELECT * FROM `users` WHERE `username` = '$username'")
            ->fetchAssoc();


        if (!$assoc)
            throw new Exception("Uživatel neexistuje.");

        if (!$this->password->verify($password, $assoc['password']))
            throw new Exception("Chybné heslo.");

        return $this->user->encapsulate($assoc['id'], $assoc['role'], ['username' => $username]);
    }
}
