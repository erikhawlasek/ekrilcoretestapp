<?php

use EkrilCore\Services\Storage\Database;
use EkrilCore\Services\Security\Password;

class Registrator
{
    private $database, $password;

    public function __construct(Database $database, Password $password)
    {
        $this->database = $database;
        $this->password = $password;

        $this->database->exec("CREATE TABLE IF NOT EXISTS `users` (`id` VARCHAR ( 400 ) PRIMARY KEY NOT NULL, `username` VARCHAR 
                              ( 40 ) NOT NULL, `email` VARCHAR ( 100 ) NOT NULL, `password` VARCHAR ( 100 ) NOT NULL, `role` VARCHAR ( 50 ) NOT NULL);");
    }

    public function register($username, $email, $password, $repeat)
    {
        if ($password === $repeat) {
            if (!$this->exists($username, $email)) {
                $password = $this->password->hash($password);

                $id = md5($username);

                $this->database->run(
                    "INSERT INTO `users` (`id`, `username`, `email`, `password`, `role`) VALUES(?,?,?,?,?);",
                    $this->encapsulate($id, $username, $email, $password, "guest")
                );
            } else {
                throw new RuntimeException("Toto uživatelské jméno nebo e-mail je již přidružený k existujícímu účtu.");
            }
        } else {
            throw new RuntimeException("Zadaná hesla se neshodují.");
        }
    }

    public function exists($username, $email)
    {
        if (
            $this->database->query("SELECT * FROM `users` WHERE `username` = '$username' OR `email` = '$email'")
            ->getAffected() !== 0
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function encapsulate($id, $username, $email, $password, $guest)
    {
        return array(
            $id,
            $username,
            $email,
            $password,
            $guest
        );
    }
}
