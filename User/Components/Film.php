<?php

class Film
{
    private $imageUrl;

    private $name;

    private $filmType;

    private $forChildren;

    private $title;

    private $description;

    private $id;

    public function __construct($id, $url, $name, $filmType, $forChildren, $title, $description)
    {
        $this->imageUrl = $url;
        $this->name = $name;
        $this->filmType = $filmType;
        $this->forChildren = $forChildren == 'people' ? "Ne" : "Ano";
        $this->title = $title == 'Yes' ? "Ano" : "Ne";
        $this->description = $description;
        $this->id = $id;
    }

    public function encapsulate()
    {
        return array(
            $this->id,
            $this->imageUrl,
            $this->name,
            $this->filmType,
            $this->forChildren,
            $this->title,
            $this->description
        );
    }
}
