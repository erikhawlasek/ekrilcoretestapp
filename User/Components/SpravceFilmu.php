<?php

use EkrilCore\Services\Storage\Database;

class SpravceFilmu
{
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;

        $this->database->exec(
            "CREATE TABLE IF NOT EXISTS `filmy` (`id` VARCHAR( 100 ) PRIMARY KEY,
        `imageUrl` VARCHAR( 100 ), `name` VARCHAR( 100 ), `filmType` VARCHAR( 100 ), `forChildren` VARCHAR( 3 ),
        `title` VARCHAR( 100 ), `description` VARCHAR( 600 ));"
        );
    }

    public function saveToRemote(Film $film)
    {
        $package = $film->encapsulate();

        $this->database->run(
            "INSERT INTO `filmy` (`id`, `imageUrl`, `name`, `filmType`, `forChildren`,
                                                   `title`, `description`) VALUES(?,?,?,?,?,?,?)",
            $package
        );
    }

    public function fetchAllFromRemote()
    {
        return $this->database->query("SELECT * from `filmy`")->fetchAll();
    }

    public function fetchById($id)
    {
        if ($this->database->query("SELECT * FROM `filmy` WHERE `id` = '$id'") !== false)
            return $this->database->fetch();
        else
            return false;
    }

    public function remove($id)
    {
        $this->database->query("DELETE FROM `filmy` where `id` = '$id'");
    }

    public function update(Film $film)
    {
        $package = $film->encapsulate();

        array_push($package, $package[0]);

        $this->database->run("UPDATE `filmy` SET `id` = ?, `imageUrl` = ?, `name` = ?, 
                             `filmType` = ?, `forChildren` = ?, `title` = ?, `description` = ?
                             WHERE `id` = ?", $package);
    }
}
