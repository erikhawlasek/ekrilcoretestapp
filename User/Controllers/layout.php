<!DOCTYPE html>

<html>

<head>
    <link rel="stylesheet" type="text/css" href="/user/assets/css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <?php $this->renderCss(); ?>
    <title>Filmy | <?php $this->renderTitle() ?></title>
</head>

<body>
    <div class="upper">
        <span>
            <?php
            if (!isset($userOpts['logged']))
                echo 'Chcete publikovat filmy? <a href="/registrace">ZAREGISTRUJTE SE</a>';
            else
                echo 'Chybí Váš oblíbený film v seznamu? Klikněte zde na <a href="/filmy/pridat">PŘIDAT FILM</a>';
            ?>
        </span>
        <div class="right">
            <?php
            if (!isset($userOpts['logged'])) {
                echo '
                    <form method="POST" action="/prihlaseni">
                        <input type="text" placeholder="Jméno" name="username">
                        <input type="password" placeholder="Heslo" name="password">
                        <a href="javascript:void(0)" onclick="parentNode.submit();">PŘIHLÁSIT</a>
                    </form>
                    ';
            } else {
                echo '
                    <form method="POST" action="/odhlaseni">
                        <a href="javascript:void(0)" onclick="parentNode.submit();">ODHLÁSIT SE</a>
                    </form>
                    ';
            }
            ?>
        </div>
        <div class="re"></div>
    </div>

    <div class="content">
        <div class="card-title">
            <p><?php $this->renderTitle(); ?></p>
        </div>
        <div class="card-content">
            <?php $this->renderPage(); ?>
        </div>
    </div>
</body>

<?php $this->renderScripts(); ?>

</html>