<div class="searchGroup">
    <input type="text" id="search" placeholder="Hledejte film...">
    <select id="select">
        <option value="-">-</option>
        <option value="Animovany">Animovany</option>
        <option value="Sci-Fi">Sci-Fi</option>
    </select>
    <div id="container">
    </div>
</div>

<script type="text/javascript">
    window.films = <?= json_encode($films); ?>;
</script>