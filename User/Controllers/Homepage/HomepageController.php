<?php

use EkrilCore\Services\Manage\User;

class HomepageController extends BaseController
{
    private $filmManager, $registrator;

    public function __construct(User $user,SpravceFilmu $filmManager, Registrator $registrator)
    {
        parent::__construct($user);
        $this->filmManager = $filmManager;
        $this->registrator = $registrator;
    }

    public function processDefault()
    {
        $this->addCss(['default.css']);
        $this->addScripts(['process.js']);
        $this->pageData['films'] = $this->filmManager->fetchAllFromRemote();
        $this->setTitle('Seznam');
    }

    public function processPrihlaseni()
    {
        if ($this->user->isLoggedIn())
            $this->redirect("/");

        if (!empty($_POST)) {
            extract($_POST);
            try {
                $this->user->login(array(htmlspecialchars($username), htmlspecialchars($password)));
            } catch (Exception $e) {
            }

            $this->redirect("/");
        }
    }

    public function processOdhlaseni()
    {
        if($this->user->isLoggedIn())
            $this->user->logout();

        $this->redirect("/");
    }

    public function processRegistrace()
    {
        $this->setTitle('Registrace');

        if (!empty($_POST)) {
            extract($_POST);

            $password = htmlspecialchars($password);

            $repeat = htmlspecialchars($repeat);

            $username = htmlspecialchars($username);

            $email = htmlspecialchars($email);

            try {
                $this->registrator->register($username, $email, $password, $repeat);
            } catch (RuntimeException $e) {
            }
        }
    }

    public function processChyba()
    {
        $this->setTitle('Chyba, něco se stalo...');
    }
}
