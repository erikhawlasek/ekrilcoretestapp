<?php

use EkrilCore\Services\Manage\User;

class FilmyController extends BaseController
{
    private $database;

    private $filmManager;

    public $film;

    public function __construct(User $user, SpravceFilmu $filmManager)
    {
        parent::__construct($user);
        $this->filmManager = $filmManager;
    }

    public function processPridat()
    {
        if (!empty($_POST)) {
            try {
                extract($_POST);

                $base = trim(explode(',', $base)[1]);

                $binary = base64_decode($base);

                $fn = uniqid();

                file_put_contents(USER . '/assets/images/' . $fn . ".png", $binary);

                $this->filmManager->saveToRemote((new Film($fn, '/User/assets/images/' . $fn . ".png", $filmName, $filmType, $ageCheck, $title, $description)));

                $this->redirect("/");
            } catch (Exception $e) {
            }
        }
        $this->addScripts(['preloader.js']);
        $this->addCss(['formy.css']);
        $this->setTitle('Přidání');
    }

    public function processSmazat()
    {
        $this->filmManager->remove($this->getParameterId());
        $this->redirect("/");
    }

    public function processUpravit()
    {
        if (!($e = $this->pageData['film'] = $this->filmManager->fetchById($this->getParameterId())))
            $this->redirect('/chyba');

        if (!empty($_POST)) {
            extract($_POST);

            if (!empty($base)) {
                $base = trim(explode(',', $base)[1]);

                $binary = base64_decode($base);

                $fn = uniqid();

                file_put_contents(USER . '/assets/images/' . $fn . ".png", $binary);

                $this->filmManager->update((new Film($e[0], '/User/assets/images/' . $fn . ".png", $filmName, $filmType, $ageCheck, $title, $description)));
            } else {
                $this->filmManager->update((new Film($e[0], $e[1], $filmName, $filmType, $ageCheck, $title, $description)));
            }

            $this->redirect("/");
        }

        $this->addScripts(['preloader.js']);
        $this->addCss(['formy.css']);
        $this->setTitle('Upravit');
    }

    public function processDetail()
    {
        if (!($this->pageData['film'] = $this->filmManager->fetchById($this->getParameterId())))
            $this->redirect('/chyba');

        $this->addCss(['detail.css']);
        $this->addScripts(['preloader.js']);
        $this->setTitle('Detail');
    }

    public function getParameterId()
    {
        return $this->parameters[0];
    }
}
