<div class="addGroup">
    <form id="addForm" method="POST" action='/filmy/pridat'>
        <input type="text" name="filmName" id="filmName" placeholder="Název filmu">
        <select id="filmType" name="filmType">
            <option value="Animovany">Animovany</option>
            <option value="Sci-Fi">Sci-Fi</option>
        </select>
        <div id="imageField"></div>
        <input id="file" name="file" type="file">
        <div id="ff"><label for="file">Nahrát obrázek</label></div>
        <div id="buttonGroup">
            <input type="radio" id="people" name="ageCheck" value="people" checked>
            <label for="people">Pro dospělé</label>
            <input type="radio" id="childrens" name="ageCheck" value="children">
            <label for="childrens">Pro děti</label>
            <input type="checkbox" id="title" name="title" value="Yes" checked>
            <label for="title">S Titulky</label><br>
        </div>
        <textarea id="description" name="description" rows="6" cols="50"></textarea><br />
        <input type="hidden" name="base">
        <input type="submit" value="Přidat">
    </form>
    <a href="/">Hl. stránka</a>
</div>