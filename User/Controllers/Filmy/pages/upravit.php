<div class="addGroup">
    <form id="addForm" method="POST" action='/filmy/upravit/<?= $film[0] ?>'>
        <input type="text" name="filmName" id="filmName" placeholder="Název filmu" value=<?= $film[2] ?>>
        <select id="filmType" name="filmType">
            <option value="Animovany">Animovany</option>
            <option value="Sci-Fi">Sci-Fi</option>
        </select>
        <div id="imageField"><img width="400px" height="500px" src="<?= $film[1] ?>" /></div>
        <input id="file" name="file" type="file">
        <div id="ff"><label for="file">Nahrát obrázek</label></div>
        <div id="buttonGroup">
            <input type="radio" id="people" name="ageCheck" value="people" <?= $film[4] == "Ne" ? "checked" : "" ?>>
            <label for="people">Pro dospělé</label>
            <input type="radio" id="childrens" name="ageCheck" value="children" <?= $film[4] == "Ano" ? "checked" : "" ?>>
            <label for="childrens">Pro děti</label>
            <input type="checkbox" id="title" name="title" value="Yes" <?= $film[5] == "Ano" ? "checked" : "" ?>>
            <label for="title">S Titulky</label><br>
        </div>
        <textarea id="description" name="description" rows="6" cols="50" value><?= $film[6] ?></textarea><br />
        <input type="hidden" name="base" value=''>
        <input type="submit" value="Upravit">
    </form>
    <a href="/">Hl. stránka</a>
    <a href="/filmy/smazat/<?= $film[0] ?>">Smazat film</a>
</div>