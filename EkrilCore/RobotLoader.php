<?php

namespace EkrilCore;

class RobotLoader
{
    private $researchedData = array();

    private $rootDir;

    private $userDir;

    public function __construct($userDir = __DIR__ . '/../User', $rootDir = __DIR__ . '/..')
    {
        $this->userDir = $userDir;
        $this->rootDir = $rootDir;

        spl_autoload_register(function ($com) {
            if (strpos($com, 'EkrilCore\\Services') !== false || strpos($com, 'EkrilCore\\Application') !== false) {
                if (@file_exists($this->rootDir . '/' . $com . '.php'))
                    return require_once $this->rootDir . '/' . $com . '.php';
            } else if (strpos($com, '\\') !== false) {
                $com = str_replace('\\', '/', $com) . '.php';
                $key = ucwords(array_reverse(explode('/', $com)[0]));

                if (@file_exists($this->userDir . '/' . $com))
                    return require_once $this->userDir . '/' . $com;
                else if (array_key_exists($key, $this->researchedData)) {
                    foreach ($this->researchedData[$key] as $dest) {
                        if (strpos($dest, $com) !== false) {
                            return require_once $dest;
                        }
                    }
                }
            } else if (array_key_exists(ucwords($com . '.php'), $this->researchedData)) {
                return require_once $this->researchedData[ucwords($com . '.php')][0];
            } else if (preg_match('/Controller$/', $com)) {
                $name = substr($com, 0, strlen($com) - 10);
                if (@file_exists($this->userDir . '/Controllers/' . $name . '/' . $com . '.php'))
                    return require_once $this->userDir . '/Controllers/' . $name . '/' . $com . '.php';
            }
        });
        return $this;
    }

    public function visit($dest)
    {
        if (@is_dir($dest)) {
            $explored = array_diff(scandir($dest), ['.', '..']);

            foreach ($explored as $item) {
                $this->visit($dest . '/' . $item);
            }
        } else {
            $fn = ucwords(array_reverse(explode('/', $dest))[0]);

            if (!array_key_exists($fn, $this->researchedData))
                $this->researchedData[$fn] = [];

            array_push($this->researchedData[$fn], $dest);
        }

        return $this;
    }
}
