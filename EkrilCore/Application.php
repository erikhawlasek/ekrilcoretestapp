<?php

namespace EkrilCore;

use EkrilCore\Services\Routing\Router;
use EkrilCore\Services\Routing\Route;

final class Application
{
    public $container;

    private $session = true;

    public function run()
    {
        if($this->session)
            session_start();
        (new Router($this))->setDefault((new Route('Homepage', 'default')))
            ->setError((new Route('Homepage', 'chyba')))
            ->ensureParams()
            ->buildRoute()
            ->invokeController();
    }

    public function createContainer($container)
    {
        $this->container = new $container();
        return $this;
    }

    public function chargeContainer($config)
    {
        $this->container->loadServices($config);
        return $this;
    }

    public function configureSession($launcher = true)
    {
        $this->session = $launcher;
    }
}
