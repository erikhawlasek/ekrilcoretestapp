<?php

namespace EkrilCore\Services;

abstract class Controller
{
    protected $data = array();
    protected $scripts;
    protected $csss;
    protected $hTags;
    protected $page;
    protected $layout;
    protected $pageData;
    protected $title;

    public function __construct()
    {
        $this->setLayout('layout');
        $this->scripts = [];
        $this->csss = [];
        $this->hTags = [];
        $this->pageData = [];
        $this->layoutData = [];
        $this->title = '';
    }

    public function redirect($target)
    {
        header("Location: $target");
        header("Connection: close");
        exit;
    }

    public function renderPage()
    {
        extract($this->pageData);
        require_once __DIR__ . '/../../user/Controllers/' . substr(static::class, 0, strlen(static::class) - 10) . '/pages/' . $this->page . '.php';
    }

    public function renderLayout()
    {
        extract($this->layoutData);
        require_once __DIR__ . '/../../user/Controllers/' . $this->layout . '.php';
    }

    public function renderCss()
    {
        foreach ($this->csss as $css)
            echo '<link rel="stylesheet" type="text/css" href="' . "/user/assets/css/$css" . '">';
    }

    public function renderScripts()
    {
        foreach ($this->scripts as $script)
            echo '<script src="' . "/user/assets/js/$script" . '"></script>';
    }

    public function renderHeaderTags()
    {
        foreach ($this->hTags as $hTag)
            echo $hTag;
    }

    public function addCss($csss = array())
    {
        foreach ($csss as $css)
            array_push($this->csss, $css);
    }

    public function addScripts($scripts = array())
    {
        foreach ($scripts as $script)
            array_push($this->scripts, $script);
    }

    public function addHeaderTag($tags = array())
    {
        foreach ($tags as $tag)
            array_push($this->hTags, $tag);
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function setDefaults($page, $parameters)
    {
        $this->page = $page;
        $this->parameters = $parameters;
    }

    public function renderTitle()
    {
        echo $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }
}
