<?php

namespace EkrilCore\Services\Security;

class Password
{
    public function hash($password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function needRehash($password)
    {
        return password_needs_rehash($password);
    }

    public function verify($password, $passwordHashed)
    {
        return password_verify($password, $passwordHashed);
    }
}
