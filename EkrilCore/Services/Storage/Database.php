<?php

namespace EkrilCore\Services\Storage;

use EkrilCore\Services\Storage\Config;
use PDO;

class Database
{
    private $server;

    private $user;

    private $password;

    private $database;

    private $hookUp;

    private $pdoStatement;

    public function __construct(Config $config)
    {
        $this->server = $config->getServer();
        $this->user = $config->getUser();
        $this->password = $config->getPassword();
        $this->database = $config->getDatabase();

        $this->hookUp = @new PDO("mysql:host=$this->server", $this->user, $this->password);
        $this->hookUp->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->enterDatabase($this->database);
    }

    public function run($command, $data)
    {
        $this->pdoStatement = ($this->hookUp->prepare($command))->execute($data);
        return $this;
    }

    public function fetch()
    {
        return $this->pdoStatement->fetch();
    }

    public function fetchAssoc()
    {
        return $this->pdoStatement->fetch(PDO::FETCH_ASSOC);
    }

    public function fetchAll()
    {
        return $this->pdoStatement->fetchAll();
    }

    public function getAffected()
    {
        return $this->pdoStatement->rowCount();
    }

    public function enterDatabase($database)
    {
        $this->hookUp->exec("CREATE DATABASE IF NOT EXISTS $this->database");
        $this->hookUp->exec("use $this->database");
    }

    public function exec($command)
    {
        $this->hookUp->exec($command);
    }

    public function query($command)
    {
        $this->pdoStatement = $this->hookUp->query($command);
        return $this;
    }
}
