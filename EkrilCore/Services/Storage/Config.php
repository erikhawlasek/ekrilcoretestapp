<?php

namespace EkrilCore\Services\Storage;

final class Config
{
    private $username;

    private $password;

    private $database;

    private $server;

    public function __construct()
    {
        $this->server = "localhost";
        $this->user = "root";
        $this->password = "";
        $this->database = "dbFilmu";
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getServer()
    {
        return $this->server;
    }
}
