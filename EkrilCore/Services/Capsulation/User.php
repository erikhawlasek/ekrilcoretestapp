<?php

namespace EkrilCore\Services\Capsulation;

class User
{
    private $id, $role, $data;

    public function encapsulate($id, $role, $data = [])
    {
        $this->id = $id;
        $this->role = $role;
        $this->data = $data;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getData()
    {
        return $this->data;
    }
}
