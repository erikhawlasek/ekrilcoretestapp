<?php

namespace EkrilCore\Services\Authenticator;

interface IAuthenticator
{
    public function authenticate($userData);
}