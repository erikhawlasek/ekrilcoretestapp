<?php

namespace EkrilCore\Services\Manage;

use EkrilCore\Services\Authenticator\IAuthenticator;

class User
{
    private $authenticator, $data;

    public function __construct(IAuthenticator $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    public function login($userData)
    {
        $this->data = $this->authenticator->authenticate($userData);

        if (is_a($this->data, \EkrilCore\Services\Capsulation\User::class)) {

            $_SESSION['id'] = $this->data->getId();
            $_SESSION['role'] = $this->data->getRole();
            $_SESSION['data'] = $this->data->getData();

            return true;
        } else {
            return false;
        }
    }

    public function getId()
    {
        return $_SESSION['id'];
    }

    public function getRole()
    {
        return $_SESSION['role'];
    }

    public function getData()
    {
        return $_SESSION['data'];
    }

    public function isLoggedIn()
    {
        if (isset($_SESSION['id']))
            return true;
        else
            return false;
    }

    public function logout()
    {
        session_destroy();
    }
}
