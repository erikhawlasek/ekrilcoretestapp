<?php

namespace EkrilCore\Services\Routing;

final class Router
{
    private $application;

    private $controller;

    private $page;

    private $controllerErr;

    private $pageErr;

    private $parameters;

    public function __construct($application)
    {
        $this->application = $application;
    }

    public function setDefault($route)
    {
        $this->controller = ucwords($route->controller);
        $this->page = ucwords($route->page);

        return $this;
    }

    public function setError($route)
    {
        $this->controllerErr = ucwords($route->controller);
        $this->pageErr = ucwords($route->page);

        return $this;
    }

    public function ensureParams()
    {
        $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $this->parameters = explode('/', trim(ltrim(parse_url($url)['path'], '/')));
        return $this;
    }

    public function buildRoute()
    {
        if (empty($this->parameters[0])) {
            $this->controller = 'Homepage';
            $this->page = 'default';
        } else if (!empty($this->parameters[0]) && empty($this->parameters[1])) {
            $this->controller = 'Homepage';
            $this->page = array_shift($this->parameters);
        } else {
            $this->controller = $this->clearControllerName(array_shift($this->parameters));
            $this->page = array_shift($this->parameters);
        }

        return $this;
    }

    public function redirect($target)
    {
        header("Location: $target");
        header("Connection: close");
        exit;
    }

    private function clearControllerName($name)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
    }

    public function invokeController()
    {
        if (!empty($this->controller) && !empty($this->page) && @file_exists(ROOT . '/user/Controllers/' . $this->controller . '/' . $this->controller . 'Controller.php')) {
            $this->directlyCallController($this->controller, $this->page);
        } else if (!empty($this->controllerErr) && !empty($this->pageErr) && @file_exists(ROOT . '/user/Controllers/' . $this->controllerErr . '/' . $this->controllerErr . 'Controller.php')) {
            $this->directlyCallController($this->controllerErr, $this->pageErr);
        } else {
            echo "<p>V nastavení routeru chybí defaultní stránka i chybová stránka. Proto vidíte tento nápis...</p>";
        }
    }

    private function directlyCallController($controller, $page)
    {
        $this->controller = $controller . 'Controller';

        $this->instance = $this->application->container->generate($this->controller);

        $this->instance->setDefaults($page, $this->parameters);

        if(method_exists($this->instance, "entryPoint"))
            $this->instance->entryPoint();

        if (method_exists($this->instance, "process" . ucwords($page)))
            $this->instance->{"process" . ucwords($page)}($this->parameters, $page);
        else {
            if ($this->controllerErr == "Homepage")
                $this->redirect("/" . $this->pageErr);
            else
                $this->redirect("/" . $this->controllerErr . "/" . $this->pageErr);
        }

        $this->instance->renderLayout();
    }
};
