<?php

namespace EkrilCore\Services\Routing;

final class Route
{
    private $data = array();

    public function __construct($controller, $page)
    {
        $this->data['controller'] = $controller;
        $this->data['page'] = $page;
    }

    public function __get($what)
    {
        return $this->data[$what];
    }
}
