<?php

namespace EkrilCore\Services\Inject;

use ReflectionClass;
use Exception;

class DiContainer
{
    private $services = array();

    public function getService($service)
    {
        if ($this->has($service))
            return $this->services[$service];
        else
            throw new Exception("Služba nenalezena. Prosím nastavte službu.");
    }

    public function generate($class)
    {
        if ($this->has($class))
            return $this->services[$class];

        $reflector = new ReflectionClass($class);

        if (!$reflector->isInstantiable())
            throw new Exception("Snaha o generování abstraktní třídy či rozhraní");

        $constructor = $reflector->getConstructor();

        if (empty($constructor))
            return $this->services[$class] = new $class();
        else {
            $parameters = $constructor->getParameters();

            if (empty($parameters))
                return $this->services[$class] = new $class();
            else {
                try {

                    $preparator = array();

                    foreach ($parameters as $parameter) {
                        array_push($preparator, $this->generate(strval($parameter->getType())));
                    }

                    return $this->services[$class] = $reflector->newInstanceArgs($preparator);
                } catch (Exception $e) {
                    echo $e;
                }
            }
        }
    }

    public function has($service)
    {
        return array_key_exists($service, $this->services);
    }

    public function loadServices($config)
    {
        $loader = include $config . '.php';

        foreach ($loader as $service => $generator) {
            $this->services[$service] = $generator($this);
        }
    }
}
