<?php

return [
    EkrilCore\Services\Authenticator\IAuthenticator::class => function ($container) {
        return $container->generate(Autentifikator::class);
    }
];
